<?php include __DIR__ . "/../../inc/header.php"; ?>
<!--/src/controllers/UploadController.php-->
<main class="">
    <div class="container">
        <div class="row">
            <form id="uploadForm" method="POST" action="/src/pages/file/content.php" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="fld-file">Upload a File:</label>
                    <input type="file" id="fld-file" name="file" accept=".txt" required/>
                    <p>*Accepts only '.txt' files</p>
                </div>

                <button class="btn btn-primary" type="submit">Upload</button>
            </form>
        </div>
    </div>
</main>

<?php include __DIR__ . "/../../inc/footer.php"; ?>
<?php include __DIR__ . "/../../inc/scripts.php"; ?>
<?php include __DIR__ . "/../../inc/end.php"; ?>

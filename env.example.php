<?php
$variables = [
    'DB_HOST' => '',
    'DB_USERNAME' => '',
    'DB_PASSWORD' => '',
    'DB_NAME' => '',
    'DB_PORT' => '',
    'APP_DEBUG' => false
];

foreach($variables as $key => $value) {
    putenv("$key=$value");
}
?>
$(document).ready(function() {
    const rainbow = ['red', 'orange', 'yellow', 'green', 'blue', 'indigo', 'violet'];
    var colorIndex = 0;

    document.addEventListener('selectstart', function() {
        if(colorIndex >= rainbow.length) colorIndex = 0;
        else colorIndex = colorIndex + 1;

        $("body").css("--select-bg", rainbow[colorIndex]);
    });
});
<?php

if(file_exists(__DIR__ . '/env.php')) {
    include __DIR__ . '/env.php';
}

if(!function_exists('env')) {
    function env($key, $default = null) {
        $value = getenv($key);

        if($value === false) {
            return $default;
        }

        return $value;
    }
}

require_once 'src/classes/Request.php';
require_once 'src/classes/Router.php';

require_once 'src/classes/Database.php';

require_once 'src/classes/Client.php';
require_once 'src/classes/File.php';
require_once 'src/classes/Chat.php';
require_once 'src/classes/Message.php';

?>
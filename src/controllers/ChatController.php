<?php

include __DIR__ . "/StandardController.php";

class ChatController extends StandardController {

    public static function create(){
        $conn = Database::getInstance();

        $query = "INSERT INTO chats (date_created, date_last_action) VALUES (:date_created, :date_last_action)";
        $stmt = $conn->prepare($query);
        $stmt->bindValue(':date_created', date('Y-m-d H:i:s'));
        $stmt->bindValue(':date_last_action', date('Y-m-d H:i:s'));
        $response = $stmt->execute();

        if($response) {
            $chat_id = $conn->lastInsertId();

            //Insert Into Pivot Table all belonging users
            $query = "INSERT INTO chat_client (chat_id, client_id) VALUES (:chat_id_1, :client_id_1), (:chat_id_2, :client_id_2)";
            $stmt = $conn->prepare($query);
            $stmt->bindValue(':chat_id_1', $chat_id);
            $stmt->bindValue(':client_id_1', $_SESSION['id']);
            $stmt->bindValue(':chat_id_2', $chat_id);
            $stmt->bindValue(':client_id_2', $_POST['client_id']);
            $response = $stmt->execute();

            echo json_encode(['chat_id' => $chat_id]);
            exit;

        }
    }

    //TODO consertar mensagens duplicadas
    public static function getChat(int $id) {
        $conn = Database::getInstance();

        $query = "SELECT * FROM messages JOIN clients WHERE messages.chat_id = " . $id; // . "ORDER BY messages.date_sent";
        return $conn->query($query)->fetchAll();
    }

    public static function newMessage() {
        $conn = Database::getInstance();

        $query = "INSERT INTO messages (chat_id, sender_id, content, date_sent) VALUES (:chat_id, :sender_id, :content, :date_sent)";
        $stmt = $conn->prepare($query);
        $stmt->bindValue(':chat_id', $_POST['chat_id']);
        $stmt->bindValue(':sender_id', $_POST['sender_id']);
        $stmt->bindValue(':content', $_POST['message']);
        $stmt->bindValue(':date_sent', date('Y-m-d H:i:s'));
        $response = $stmt->execute();

//        echo json_encode(['chat_id' => $chat_id]);
        exit;
    }
}

ChatController::callMethod();

?>
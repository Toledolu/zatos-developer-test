<?php

class File {
    private $handle;
    private $file;
    private $content;

    public function __construct(string $path) {
        $this->file = $path;
    }

    public function load(string $mode = 'r') {
        if($this->handle = fopen($this->file, $mode)) {
            return $this;
        }
        return null;
    }

    public function read() {
        if($read = fread($this->handle, filesize($this->file))) {
           fclose($this->handle);
           $this->content = $read;
           return true;
        }

        fclose($this->handle);
        return false;
    }

    public function getContent() {
        return $this->content;
    }
}

?>
<?php session_start(); ?>

<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <title>Zatos Developer Test</title>

    <link rel="icon" type="image/png" href="">
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo '/assets/css/app.css?version=' . date('h:i:s'); ?>" rel="stylesheet">

</head>
<body>
<div id="app">

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container">
            <a class="navbar-brand" href="/">Home</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="navbar-collapse collapse">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="/src/pages/client/register.php">Add Client</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/recent">Recent Clients</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/src/pages/file/upload.php">Upload File</a>
                    </li>
                </ul>
                <ul class="navbar-nav ml-auto">
                    <?php if(isset($_SESSION['id'])){ ?>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="accountDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <?php echo $_SESSION['name']; ?>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="accountDropdown">
                            <form id="logoutForm" method="POST" action="/src/controllers/AuthController.php" style="display:none;">
                                <input type="hidden" name="method" value="logout">
                            </form>
                            <a class="dropdown-item" onclick="document.getElementById('logoutForm').submit();">Logout</a>
                        </div>
                    </li>
                    <?php } else { ?>
                        <li class="nav-item"><a class="nav-link" href="/src/pages/login.php">Login</a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
    </nav>
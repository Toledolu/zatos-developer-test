<?php include __DIR__ . "/../../inc/header.php"; ?>
<?php
try {
    $clients = Client::getRecentClients();
} catch(Exception $e) {
    if(env('APP_DEBUG')) {
        echo '<pre>';
        print_r($e);
        echo '</pre>';
    }
}
?>

<main class="">
    <div class="container">
        <h2>All Clients:</h2>
        <div class="row">
            <div class="col-12">
                <table class="table table-striped">
                    <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($clients as $client): ?>
                        <tr>
                            <th scope="row"><a href="/src/pages/client/view.php?id=<?php echo $client['id'] ?>" class="btn btn-link"><?php echo $client['id'] ?></a></th>
                            <td><a href="/src/pages/client/view.php?id=<?php echo $client['id'] ?>" class="btn btn-link"><?php echo $client['name'] ?></a></td>
                            <td>
                                <a href="/src/pages/client/edit.php?id=<?php echo $client['id'] ?>" class="btn btn-info">Editar</a>

                                <form id="<?php echo 'deleteForm-' . $client['id']; ?>" method="POST" action="/src/controllers/ClientController.php" style="display:none;">
                                    <input type="hidden" name="method" value="delete">
                                    <input type="hidden" name="client_id" value="<?php echo $client['id']; ?>">
                                </form>
                                <button id="deleteBTN-<?php echo $client['id'] ?>" class="btn btn-danger">Excluir</button>
                            </td>
                        </tr>
                    <?php endforeach ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>

<?php include __DIR__ . "/../../inc/footer.php"; ?>
<?php include __DIR__ . "/../../inc/scripts.php"; ?>

<script>
    $(document).ready(function() {
        $("[id^=deleteBTN-").click(function(event) {
            event.preventDefault();
            var form = $(event.currentTarget).siblings("form");

            $.ajax({
                url : "/src/controllers/ClientController.php",
                type : 'POST',
                data : form.serialize(),
                beforeSend : function(){
                    alert("AJAX ENVIANDO...");
                }
            })
                .done(function(response){
                    window.location.href = "/";
                })
                .fail(function(jqXHR, textStatus, response){
                    alert(response);
                });
        });
    });
</script>

<?php include __DIR__ . "/../../inc/end.php"; ?>

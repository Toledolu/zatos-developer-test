<?php

session_start();

include __DIR__ . "/StandardController.php";

class AuthController extends StandardController {

    public static function login() {
        $query = "SELECT id, name FROM clients WHERE email = :email AND password = :password";
        $conn = Database::getInstance();
        $stmt = $conn->prepare($query);
        $stmt->bindValue(':email', $_POST['email']);
        $stmt->bindValue(':password', md5($_POST['password']));
        $stmt->execute();
        $response = $stmt->fetch();

        if($response) {
            $query = "UPDATE clients SET date_last_access = :value WHERE id = :id";

            $stmt = $conn->prepare($query);
            $stmt->bindValue(':id', $response['id']);
            $stmt->bindValue(':value', date('Y-m-d H:i:s'));
            $stmt->execute();

            $_SESSION['id'] = $response['id'];
            $_SESSION['name'] = $response['name'];

            header('location: /');
            exit;
        }

        header('Location: ' . $_SERVER['HTTP_REFERER']);
        exit;
    }

    public static function register() {
        $conn = Database::getInstance();

        $query = "INSERT INTO clients (name, password, cpf, cnpj, email, lat, lng, date_created, date_modification) VALUES (:name, :password, :cpf, :cnpj, :email, :lat, :lng, :date_created, :date_modification)";
        $stmt = $conn->prepare($query);
        $stmt->bindValue(':name', $_POST['name']);
        $stmt->bindValue(':password', md5($_POST['password']));
        $stmt->bindValue(':cpf', $_POST['cpf'] ?? null);
        $stmt->bindValue(':cnpj', $_POST['cnpj'] ?? null);
        $stmt->bindValue(':email', $_POST['email']);
        $stmt->bindValue(':lat', $_POST['lat']);
        $stmt->bindValue(':lng', $_POST['lng']);
        $stmt->bindValue(':date_created', date('Y-m-d H:i:s'));
        $stmt->bindValue(':date_modification', date('Y-m-d H:i:s'));
        $response = $stmt->execute();

        //TODO Fix response JSON to AJAX
        //header('Content-type: application/json');
        //echo json_encode(['data' => $response]);
        //exit;
    }

    function logout() {
        unset($_SESSION['id']);
        unset($_SESSION['name']);
        header('location: /');
    }
}

AuthController::callMethod();

?>
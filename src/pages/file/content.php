<?php include __DIR__ . "/../../../autoload.php"; ?>

<?php include __DIR__ . "/../../inc/header.php"; ?>

<?php


if($_SERVER["REQUEST_METHOD"] == "POST") {

    if(isset($_FILES["file"]) && $_FILES["file"]["error"] == 0) {
        $file_name = $_FILES["file"]["name"];
        $file_type = $_FILES["file"]["type"];
        $file_size = $_FILES["file"]["size"];

        $allowed = ["txt" => "text/plain"];
        $ext = pathinfo($file_name, PATHINFO_EXTENSION);
        if(!array_key_exists($ext, $allowed)) die("Error: Please select a valid file format.");

        $maxsize = 5 * 1024 * 1024;
        if($file_size > $maxsize) die("Error: File size is larger than the allowed limit.");

        if(in_array($file_type, $allowed)){
            $file_name = $_SERVER['DOCUMENT_ROOT'] . "/assets/uploads/" . date('m-d-Y_hia') . "." . $ext;
            move_uploaded_file($_FILES["file"]["tmp_name"], $file_name);
        } else{
            die("Error: There was a problem uploading your file. Please try again.");
        }

        $file = new File($file_name);
        $file->load();
        $file->read();
        $content = $file->getContent();
    } else{
        echo "Error: " . $_FILES["file"]["error"];
    }
}

?>

    <main class="">
        <div class="container">
            <div class="row">
                <div class="col-10 offset-1">
                    <div class="row">
                        <h2>Conteúdo do Arquivo: <?php echo $_FILES["file"]["name"]; ?></h2>
                    </div>
                    <div class="row">
                        <p><?php echo $content; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </main>

<?php include __DIR__ . "/../../inc/footer.php"; ?>
<?php include __DIR__ . "/../../inc/scripts.php"; ?>
<?php include __DIR__ . "/../../inc/end.php"; ?>
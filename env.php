<?php
$variables = [
    'DB_HOST' => '192.168.10.10',
    'DB_USERNAME' => 'homestead',
    'DB_PASSWORD' => 'secret',
    'DB_NAME' => 'zatos',
    'DB_PORT' => '3306',
    'APP_DEBUG' => true
];

foreach($variables as $key => $value) {
    putenv("$key=$value");
}
?>
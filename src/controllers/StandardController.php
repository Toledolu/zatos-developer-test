<?php

include __DIR__ . "/../../autoload.php";

class StandardController {

    public static function callMethod() {
//        echo "<pre>";
//        var_dump($_POST['method']);

        if(!empty($_POST['method']) && method_exists(get_called_class(), $_POST['method'])) {
            call_user_func([get_called_class(), $_POST['method']]);
        }
    }

}

?>
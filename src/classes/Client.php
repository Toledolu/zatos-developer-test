<?php

class Client {
    public $id;
    public $name;
    public $cpf;
    public $cnpj;
    public $email;
    public $lat;
    public $lng;
    public $date_created;
    public $date_modification;
    public $date_last_access;

    public function __construct() {

    }

    public static function getAllClients() {
        $query = "SELECT * FROM clients ORDER BY id";
        $conn = Database::getInstance();
        return $conn->query($query)->fetchAll();
    }

    public static function getRecentClients() {
        $query = "SELECT * FROM clients WHERE date_last_access >= :date ORDER BY id";
        $conn = Database::getInstance();
        $stmt = $conn->prepare($query);
        $stmt->bindValue(':date', date('Y-m-d H:i:s', strtotime('-5 minutes', strtotime(date("Y-m-d H:i:s")))));
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function getClient(int $id) {
        $query = "SELECT * FROM clients WHERE id = :id";
        $conn = Database::getInstance();
        $stmt = $conn->prepare($query);
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        return $stmt->fetch();
    }

    public static function delete(int $id) {
        $query = "DELETE FROM clients WHERE id = :id";
        $conn = Database::getInstance();
        $stmt = $conn->prepare($query);
        $stmt->bindValue(':id', $id);
        $stmt->execute();
    }
}

?>
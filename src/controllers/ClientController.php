<?php

session_start();

include __DIR__ . "/StandardController.php";

class ClientController extends StandardController {

    public static function update() {
        $conn = Database::getInstance();

        $query = "UPDATE clients SET name = :name, password = :password, cpf = :cpf, cnpj = :cnpj, email = :email, lat = :lat, lng = :lng, date_modification = :date_modification WHERE id = :id";
        $stmt = $conn->prepare($query);
        $stmt->bindValue(':id', $_POST['id']);
        $stmt->bindValue(':name', $_POST['name']);
        $stmt->bindValue(':password', md5($_POST['password']));
        $stmt->bindValue(':cpf', $_POST['cpf'] ?? null);
        $stmt->bindValue(':cnpj', $_POST['cnpj'] ?? null);
        $stmt->bindValue(':email', $_POST['email']);
        $stmt->bindValue(':lat', $_POST['lat']);
        $stmt->bindValue(':lng', $_POST['lng']);
        $stmt->bindValue(':date_modification', date('Y-m-d H:i:s'));
        $response = $stmt->execute();
    }

    public function delete() {
        if ($_POST['client_id']) {
            $conn = Database::getInstance();
            Client::delete($_POST['client_id']);
            header('Location: /src/pages/home.php');
            exit;
        }
    }
}

ClientController::callMethod();
?>
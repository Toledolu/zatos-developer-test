<?php include __DIR__ . "/../../inc/header.php"; ?>

    <main class="">
        <div class="container">
            <div class="row">
                <div class="col-10 offset-1">
                    <form id="registerForm" method="POST" action="/src/controllers/AuthController.php">
                        <input type="hidden" name="method" value="update">
                        <input type="hidden" name="id" value="<?php echo $_GET['id']; ?>">
                        <div class="form-group">
                            <label for="fld-name">Name</label>
                            <input class="form-control" type="text" id="fld-name" name="name" placeholder="John Doe" required value="Teste"/>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-6">
                                    <label for="fld-email">Email</label>
                                    <input class="form-control" type="email" id="fld-email" name="email" placeholder="john.doe@gmail.com" value="teste@teste.com.br" required/>
                                </div>
                                <div class="col-6">
                                    <label for="fld-password">Password</label>
                                    <input class="form-control" type="password" id="fld-password" name="password" value="secret" required/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-6">
                                    <label for="fld-cpf">CPF</label>
                                    <input class="form-control" type="text" id="fld-cpf" name="cpf" placeholder="000.000.000-00"/>
                                </div>
                                <div class="col-6">
                                    <label for="fld-cnpj">CNPJ</label>
                                    <input class="form-control" type="text" id="fld-cnpj" name="cnpj" placeholder="00.000.000/0000-00"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-6">
                                    <label for="fld-lat">Latitude</label>
                                    <input class="form-control" type="text" id="fld-lat" name="lat" placeholder="-23.550164466" value="-23.550164466" required/>
                                </div>
                                <div class="col-6">
                                    <label for="fld-lng">Longitude</label>
                                    <input class="form-control" type="text" id="fld-lng" name="lng" placeholder="-46.633664132" value="-23.550164466" required/>
                                </div>
                            </div>
                        </div>

                        <button id="submit" class="btn btn-primary">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </main>

<?php include __DIR__ . "/../../inc/footer.php"; ?>
<?php include __DIR__ . "/../../inc/scripts.php"; ?>

    <script>
        $(document).ready(function() {
            $('#fld-cpf').mask('000.000.000-00');
            $('#fld-cnpj').mask('00.000.000/0000-00');

            $('#fld-lat').mask('-00.000000000');
            $('#fld-lng').mask('-00.000000000');

            $("#submit").click(function(event) {
                event.preventDefault();

                var form = $("#registerForm");

                $.ajax({
                    url : "/src/controllers/ClientController.php",
                    type : 'POST',
                    data : form.serialize(),
                    beforeSend : function(){
                        alert("AJAX ENVIANDO...");
                    }
                })
                .done(function(response){
                    window.location.href = "/src/pages/client/edit.php?id=<?php echo $_GET['id']; ?>";
                })
                .fail(function(jqXHR, textStatus, response){
                    alert(response);
                });
            });
        });
    </script>

<?php include __DIR__ . "/../../inc/end.php"; ?>
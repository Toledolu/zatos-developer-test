CREATE DATABASE IF NOT EXISTS zatos;

use zatos;

CREATE TABLE IF NOT EXISTS clients (
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(100) NOT NULL,
  password VARCHAR(255) NOT NULL,
  cpf VARCHAR(20) NULL,
  cnpj VARCHAR(20) NULL,
  email VARCHAR(255) UNIQUE NOT NULL,
  lat VARCHAR(255) NOT NULL,
  lng VARCHAR(255) NOT NULL,
  date_created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  date_modification TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  date_last_access TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS chats (
  id INT NOT NULL AUTO_INCREMENT,
  date_created TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  date_last_action TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS chat_client (
  id INT NOT NULL AUTO_INCREMENT,
  chat_id INT NOT NULL,
  client_id INT NOT NULL,
  PRIMARY KEY(id),
  FOREIGN KEY(chat_id) REFERENCES chats(id) ON DELETE CASCADE,
  FOREIGN KEY(client_id) REFERENCES clients(id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS messages (
  id INT NOT NULL AUTO_INCREMENT,
  chat_id INT NOT NULL,
  sender_id INT NOT NULL,
  content VARCHAR(255) NOT NULL,
  date_sent TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY(id),
  FOREIGN KEY(chat_id) REFERENCES chats(id),
  FOREIGN KEY(sender_id) REFERENCES clients(id)
);

INSERT INTO clients (name, password, cpf, cnpj, email, lat, lng, date_created, date_modification, date_last_access) VALUES
("Luiz Carlos", "5ebe2294ecd0e0f08eab7690d2a6ee69", "123.456.789-10", null, "luiz.carlos@teste.com", "-23.550164466", "-46.633664132", "2020-03-07 20:14:02", "2020-03-07 20:14:02", "2020-03-07 20:14:02"),
("Empresa XPTO", "5ebe2294ecd0e0f08eab7690d2a6ee69", null, "12.345.678/0000-10", "empresa@xpto.com", "-23.550164466", "-46.633664132", "2020-03-07 20:14:02", "2020-03-07 20:14:02", "2020-03-07 20:14:02");

INSERT INTO chats (date_created, date_last_action) VALUES
("2020-03-07 20:14:02", "2020-03-07 20:14:02");

INSERT INTO chat_client (chat_id, client_id) VALUES
(1, 1), (1, 2);

INSERT INTO messages (chat_id, sender_id, content, date_sent) VALUES
(1, 1, "Olá", "2020-03-07 20:14:02"),
(1, 1, "Teste", "2020-03-07 20:14:02"),
(1, 2, "Resposta", "2020-03-07 20:14:02");
<?php include __DIR__ . "/../../controllers/ChatController.php"; ?>
<?php include __DIR__ . "/../../inc/header.php"; ?>
<?php
try {
    $messages = ChatController::getChat($_GET['id']);

} catch(Exception $e) {
    if(env('APP_DEBUG')) {
        echo '<pre>';
        print_r($e);
        echo '</pre>';
    }
}
?>

<main class="">
    <div class="container">
        <h2>Chat :</h2>
        <div class="row">
            <div class="col-12">

                <div class="inbox_msg">
                    <div class="messages">
                        <div class="msg_history">

                            <?php foreach($messages as $message): ?>

                                <div class="<?php echo $message['sender_id'] == $_SESSION['id'] ? 'outgoing_msg' : 'incoming_msg'; ?>">
                                    <div class="msg_wrapper">
                                        <p><?php echo utf8_encode($message['content']); ?></p>
                                        <span class="time_date"><?php echo ($message['date_sent']); ?> - <?php echo ($message['name']); ?></span>
                                    </div>
                                </div>

                            <?php endforeach; ?>

                        </div>
                        <div class="type_msg">
                            <div class="input_msg_write">
                                <input id="input_message" type="text" class="write_msg" placeholder="Type a message" />
                                <button id="sendMessageBTN" class="msg_send_btn" type="button">Send</button>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</main>

<?php include __DIR__ . "/../../inc/footer.php"; ?>
<?php include __DIR__ . "/../../inc/scripts.php"; ?>

<script>
    $(document).ready(function() {
        $("#sendMessageBTN").click(function(event) {
            event.preventDefault();
            var message = $("#input_message").val();

            $.ajax({
                url : "/src/controllers/ChatController.php",
                type : 'POST',
                data : {
                    'method': 'newMessage',
                    'chat_id': <?php echo $messages[0]['chat_id']; ?>,
                    'sender_id': <?php echo $_SESSION['id']; ?>,
                    'message': message
                },
                beforeSend : function(){
                    alert("AJAX ENVIANDO...");
                }
            })
            .done(function(response){
                location.reload();
            })
            .fail(function(jqXHR, textStatus, response){
                alert(response);
            });
        });
    });
</script>

<?php include __DIR__ . "/../../inc/end.php"; ?>

<?php include __DIR__ . "/../inc/header.php"; ?>

    <main class="">
        <div class="container">
            <div class="row">
                <div class="col-10 offset-1">
                    <form id="loginForm" method="POST" action="/src/controllers/AuthController.php">
                        <input type="hidden" name="method" value="login">
                        <div class="form-group">
                            <label for="fld-email">Email</label>
                            <input class="form-control" type="email" id="fld-email" name="email" placeholder="john.doe@gmail.com"/>
                        </div>
                        <div class="form-group">
                            <label for="fld-password">Password</label>
                            <input class="form-control" type="password" id="fld-password" name="password"/>
                        </div>

                        <button class="btn btn-primary" type="submit">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </main>

<?php include __DIR__ . "/../inc/footer.php"; ?>
<?php include __DIR__ . "/../inc/scripts.php"; ?>
<?php include __DIR__ . "/../inc/end.php"; ?>

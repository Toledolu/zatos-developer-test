# Zatos Developer Test

Projeto feito para teste de desenvolvedor Zatos.

## Getting Started

### Prerequisites

Ter um servidor Web rodando e banco de dados conectado além de ter habilitado a extensão PDO com MySQL no arquivo de configuração 'php.ini' e por fim é preciso criar a pasta 'assets/uploads' pois essa pasta está no gitignore.

### Installing

Troque o nome do arquivo 'env.example.php' para 'env.php' e configure as variáveis conforme seu ambiente.

Exemplo de configuração possível:

```
    'DB_HOST' => '192.168.10.10',
    'DB_USERNAME' => 'homestead',
    'DB_PASSWORD' => 'secret',
    'DB_NAME' => 'zatos',
    'DB_PORT' => '3306',
```

Sirva a aplicação no servidor e já estará pronto para utilizar.

## Document Answers

**1:** A função ‘mysql_query’ foi depreciada nas versões mais recentes do PHP, então para fazer a conexão com Banco de Dados é possível utilizar a função ‘mysqli_query’ ou a utilização da biblioteca PDO com maior abstração. Um exemplo de como utilizar conexão via PDO está descrito na classe “Database.php”.

**2:** Para um sistema desenvolvido em PHP precisamos geralmente de um Web Server (Nginx, Apache) rodando, linguagem de marcação (XML, HTML), linguagem de estilos (CSS) e linguagem comportamental (JS).

**3:** Classe "File.php" para Leitura de Arquivos.

**4:** Ao submeter arquivo via POST para "/upload" ele é salvo e seu conteúdo exibido.

**5:** Utilização de Sessão para Logar usuários.

**6:** Função de 'autoload' para achar classes mesmo dentro de subdiretórios e então inclui-las.

**7:** CRUD classe 'Client.php' via AJAX.

**8:** Função 'getRecentClients' da classe "Client.php".

**9:** Chat entre usuários pelas classes "Chat.php", "Message.php" e "Client.php".

## Authors

* **Lucas Toledo Heilig** - [Toledolu](https://gitlab.com/Toledolu)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

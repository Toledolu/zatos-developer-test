<?php

include 'autoload.php';

$conn = Database::getInstance();
$query = file_get_contents(__DIR__ . "/assets/database/migration.sql");
$stmt = $conn->prepare($query);

if($stmt->execute()) {
    echo "Migration Executada com Successo";
} else {
    echo "Migration Failed";
}

?>